package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.util.List;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE

		// sort (case 1)
		// PLACE YOUR CODE HERE
		Service service = domController.readDocument();
		System.out.println(service);
		service.sortBy(new ComparatorTariffPriceDay());
		
		// save
		String outputXmlFile = "output.dom.xml";
		// PLACE YOUR CODE HERE
		System.out.println("Output ==> " + outputXmlFile);




		// PLACE YOUR CODE HERE

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		 //get
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser parser = factory.newSAXParser();
		SAXController handler = new SAXController(xmlFileName);

		parser.parse(new File(xmlFileName), handler);
		service = SAXController.getService();
		System.out.println(service);
		handler.writeXml();
//		SAXController.saveToXML(service, outputXmlFile);
		outputXmlFile = "output.sax.xml";
		System.out.println("Output ==> " + outputXmlFile);


		// PLACE YOUR CODE HERE



		// sort  (case 2)
		// PLACE YOUR CODE HERE
		
		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		Service service1 = staxController.readDocument();
		System.out.println(service1);
		// PLACE YOUR CODE HERE
		
		// sort  (case 3)
		// PLACE YOUR CODE HERE
		
		// save
		outputXmlFile = "output.stax.xml";
		// PLACE YOUR CODE HERE
	}

}
