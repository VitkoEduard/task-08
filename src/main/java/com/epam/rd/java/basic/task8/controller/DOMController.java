package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.Service;
import com.epam.rd.java.basic.task8.Tariff;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE
	public Service readDocument() {
		Service service = new Service();

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		try {

			dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
			DocumentBuilder db = dbf.newDocumentBuilder();

			Document doc = db.parse(new File(xmlFileName));

			doc.getDocumentElement().normalize();

			service.setName(doc.getDocumentElement().getAttribute("name"));

			NodeList list = doc.getElementsByTagName("tariff");

			FileOutputStream output = new FileOutputStream("output.dom.xml");
			writeXml(doc, output);



			for (int temp = 0; temp < list.getLength(); temp++) {

				Node node = list.item(temp);

				if (node.getNodeType() == Node.ELEMENT_NODE) {

					Element element = (Element) node;

					int id = Integer.parseInt(element.getElementsByTagName("id").item(0).getTextContent());
					String name = element.getElementsByTagName("name").item(0).getTextContent();
					int pricePerDay = Integer.parseInt(element.getElementsByTagName("pricePerDay").item(0).getTextContent());


					Tariff tariff = new Tariff(id, name,pricePerDay);
					service.addTariff(tariff);
				}
			}

		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}

		return service;
	}
	private static void writeXml(Document doc, OutputStream output) throws TransformerException {
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(output);

		transformer.transform(source, result);
	}


}
