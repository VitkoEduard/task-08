package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Tariff;

import java.util.Comparator;

public class ComparatorTariffPriceDay implements Comparator<Tariff> {

    @Override
    public int compare(Tariff o1, Tariff o2) {


        return Integer.compare(o1.getPricePerDay(), o2.getPricePerDay());
    }
}
