package com.epam.rd.java.basic.task8;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Service {

    private String name;
    private List<Tariff> tariffs;

    public Service() {
        this.tariffs = new ArrayList<>();
    }

    public Service( String name, List<Tariff> tariffs) {

        this.name = name;
        this.tariffs = new ArrayList<>();;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Tariff> getTariffs() {
        return tariffs;
    }

    public void setTariffs(List<Tariff> tariffs) {
        this.tariffs = tariffs;
    }

    public void addTariff(Tariff tariff) {
        tariffs.add(tariff);
    }
    public void sortBy(Comparator<Tariff> comparator){
        tariffs.sort(comparator);
    }

    @Override
    public String toString() {
        return "Service{" +
                "name='" + name + '\'' +
                ", tariffs=" + tariffs +
                '}';
    }
}
