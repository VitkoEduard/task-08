package com.epam.rd.java.basic.task8;

public enum XML {
    SEVICE("service"),
    TARIFF("tariff"),
    ID("id"),
    NAME("name"),

    PRICE_PER_DAY("pricePerDay");

    private String value;

    XML(String value) {
        this.value = value;
    }

    public boolean equalsTo(String name) {
        return value.equals(name);
    }

    public String value() {
        return value;
    }
}
