package com.epam.rd.java.basic.task8.controller;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


import com.epam.rd.java.basic.task8.Service;
import com.epam.rd.java.basic.task8.Tariff;
import com.epam.rd.java.basic.task8.XML;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.*;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;




public class SAXController extends DefaultHandler {

    private String xmlFileName;

    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }


    List<Tariff> tariffs;
    private static Service service= new Service();



    public static Service getService() {
        return service;
    }

    public List<Tariff> getTariffs() {
        return tariffs;
    }

    @Override
    public void startDocument() {
        tariffs = new ArrayList<>();
    }

    private int id;
    private String name;
    private int pricePerDay;
    private String lastElementName;

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        lastElementName = qName;
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        String information = new String(ch, start, length);

        information = information.replace("\n", "").trim();
        if (!information.isEmpty()) {
            if (lastElementName.equals("id")) {
                id = Integer.parseInt(information);
            }
            if (lastElementName.equals("name")) {
               name = information;
            }

            if (lastElementName.equals("pricePerDay")) {
                pricePerDay = Integer.parseInt(information);
            }


        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        if ((name != null && !name.isEmpty()) &&
                id != 0 && (pricePerDay != 0)) {

           service.addTariff(new Tariff(id, name, pricePerDay));
            id = 0;
           name = null;
           pricePerDay = 0;
        }
    }

    public static Document getDocument(Service service) throws ParserConfigurationException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document document = db.newDocument();
        Element tElement = document.createElement(XML.SEVICE.value());
        document.appendChild(tElement);

        for (Tariff tariff : service.getTariffs()) {

            Element tariffElement = document.createElement(XML.TARIFF.value());
            tElement.appendChild(tariffElement);

            Element nameElement = document.createElement(XML.NAME.value());
            nameElement.setTextContent(tariff.getName());
            tariffElement.appendChild(nameElement);

            Element idElement = document.createElement(XML.ID.value());
            idElement.setTextContent(Integer.toString(tariff.getId()));
            tariffElement.appendChild(idElement);


            Element priceElement = document.createElement(XML.PRICE_PER_DAY.value());
            priceElement.setTextContent(String.valueOf(tariff.getPricePerDay()));
            tariffElement.appendChild(priceElement);
        }
        return document;
    }

    public static void saveToXML(Service service, String xmlFileName)
            throws ParserConfigurationException, TransformerException {
        saveToXML(getDocument(service), xmlFileName);
    }

    public static void saveToXML(Document document, String xmlFileName) throws TransformerException {
        StreamResult result = new StreamResult(new File(xmlFileName));
        TransformerFactory tf = TransformerFactory.newInstance();
        javax.xml.transform.Transformer t = tf.newTransformer();
        t.setOutputProperty(OutputKeys.INDENT, "yes");
        DOMSource source = new DOMSource(document);

        t.transform(source, result);
    }

    public Service readSaxParser() {
        Service service = new Service();
        SAXParserFactory factory = SAXParserFactory.newInstance();

        try (InputStream is = getXMLFileAsStream()) {
            SAXParser saxParser = factory.newSAXParser();
            // parse XML and map to object, it works, but not recommend, try JAXB
            SAXController handler = new SAXController(xmlFileName);
            XMLReader xmlReader = saxParser.getXMLReader();
            saxParser.parse(xmlFileName, handler);
            // print all
            List<Tariff> tariffs = getTariffs();
            String output = new String("output.dom.xml");
            saveToXML(service,output);
            for (int i = 0; i < tariffs.size(); i++) {
                service.addTariff(tariffs.get(i));
            }
            tariffs.forEach(System.out::println);

        } catch (ParserConfigurationException | SAXException | IOException | TransformerException e) {
            e.printStackTrace();
        }
        return service;
    }

    private static InputStream getXMLFileAsStream() {
        return SAXController.class.getClassLoader().getResourceAsStream("input.xml");
    }
    public void writeXml(){
        try {
            FileOutputStream output = new FileOutputStream("output.sax.xml");
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(new File(xmlFileName));
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(output);

            transformer.transform(source, result);
        } catch (ParserConfigurationException | SAXException | IOException | TransformerException e) {
            e.printStackTrace();
        }

    }

    }

